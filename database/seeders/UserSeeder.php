<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        foreach (range(1, 3) as $key) {
            DB::table('users')->insert([
                'name' => $faker->name(),
                'email' => 'consultant-' . $key . '@iceberg-digital.co.uk',
                'password' => Hash::make('password'),
            ]);
        }
    }
}
