<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->string('postcode');
            $table->unsignedInteger('distance')->comment('in meters')->nullable();
            $table->unsignedInteger('duration')->comment('in seconds')->nullable();
            $table->dateTime('appointment_time');
            $table->dateTime('estimated_departure_time');
            $table->dateTime('estimated_availability_time');
            $table->foreignId('contact_id')->constrained('contacts')->nullable();
            $table->foreignId('created_by')->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
