# Iceberg Digital PHP Coding Challenge: RESTful API with Laravel

This application is deployed on Heroku  : https://iceberg-digital-api-challenge.herokuapp.com/

- [Iceberg Digital PHP Coding Challenge: RESTful API with Laravel](#iceberg-digital-php-coding-challenge-restful-api-with-laravel)
  - [About](#about)
  - [Infrastructre](#infrastructre)
    - [Architecture](#architecture)
    - [Authentication](#authentication)
    - [Third parties](#third-parties)
  - [Installation](#installation)
  - [Endpoints](#endpoints)
    - [Authentication endpoints](#authentication-endpoints)
      - [`POST api/auth/register`](#post-apiauthregister)
      - [`POST api/auth/login`](#post-apiauthlogin)
      - [`POST api/auth/logout`](#post-apiauthlogout)
      - [`POST api/auth/refresh`](#post-apiauthrefresh)
      - [`GET api/auth/user-profile`](#get-apiauthuser-profile)
    - [Appointment endpoints](#appointment-endpoints)
      - [`POST api/appointment/create`](#post-apiappointmentcreate)
      - [`PATCH api/appointment/update/{appointmentId}`](#patch-apiappointmentupdateappointmentid)
      - [`DELETE api/appointment/delete/{appointmentId}`](#delete-apiappointmentdeleteappointmentid)
      - [`GET api/appointment/get-appointments`](#get-apiappointmentget-appointments)
  - [TODO](#todo)
## About

This application is made to make easier of a real-estate firm's life. Via the API that application provides, consultants of the firm can do several operations;

- Register and login to the app,
- Create an appointment,
- Update an appointment,
- Delete an appointment,
- List appointments (all appointaments or filtered by creation time)

## Infrastructre

### Architecture

[Repository pattern](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/infrastructure-persistence-layer-design#the-repository-pattern) is used to access data sources in the application.
### Authentication

The app uses an implementation of [JWT](https://jwt.io/) for Laravel to authenticate users. When the user logins to the app with valid credentials, the app returns a signed access token back to the user, which is valid for a limited time period to use in further requests.  When the token expires, the app returns a new access token to the user in the Authorization header of the response with very next request. So, client can check if the response has a new access token and keep user signed in.

### Third parties

- Libraries
  - [tymon/jwt-auth](https://github.com/tymondesigns/jwt-auth)
- APIs
  - [Google Distance Matrix API](https://developers.google.com/maps/documentation/distance-matrix/overview): To calculate distance and duration between postcodes
  - [Postcodes UK postcodes API](https://postcodes.io/): To validate and generate postcodes

## Installation

- Clone the repository
- Install dependencies via composer
  ```bash
  $ composer install
  ```
- Generate JWT secret via following command
  ```bash 
  $ php artisan jwt:secret
  ```
- Run docker container via command below
  ```bash
  $ vendor/sail up -d #-d for detached mode
  ```
- Run migrations
  ```bash
  $ vendor/sail artisan migrate
  ```
- Run seeds
  ```bash
  $ vendor/sail artisan db:seed
  ```

## Endpoints

[Click to access Postman collection](https://www.getpostman.com/collections/63a0ff8da6b9c47d4358
) 
| Method   | URI                                    |
| -------- | -------------------------------------- |
| `POST`   | api/appointment/create                 |
| `DELETE` | api/appointment/delete/{appointmentId} |
| `GET`    | api/appointment/get-appointments       |
| `PATCH`  | api/appointment/update/{appointmentId} |
| `POST`   | api/auth/login                         |
| `POST`   | api/auth/logout                        |
| `POST`   | api/auth/refresh                       |
| `POST`   | api/auth/register                      |
| `GET`    | api/auth/user-profile                  |

### Authentication endpoints
#### `POST api/auth/register`
**Parameters**
| Name                  | Required   | Type     | Description                |
| --------------------- | ---------- | -------- | -------------------------- |
| name                  | `required` | `string` | User name                  |
| email                 | `required` | `string` | User email                 |
| password              | `required` | `string` | User password              |
| password_confirmation | `required` | `string` | User password confirmation |

**Response**
```json
{
    "message": "User successfully registered",
    "user": {
        "name": "Dummy User",
        "email": "dummy@iceberg.com",
        "updated_at": "2021-12-11T21:13:38.000000Z",
        "created_at": "2021-12-11T21:13:38.000000Z",
        "id": 4
    }
}
```

#### `POST api/auth/login`
**Parameters**
| Name     | Required   | Type     | Description   |
| -------- | ---------- | -------- | ------------- |
| email    | `required` | `string` | User email    |
| password | `required` | `string` | User password |

**Response**
```json
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODg4OFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTYzOTI1Njc0MiwiZXhwIjoxNjM5MjYwMzQyLCJuYmYiOjE2MzkyNTY3NDIsImp0aSI6Iks2TkJtSENmckNtck1IczYiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ._-x0iK5GySvxeJlId9t8J3UPKg-7YqZsG-A_GQtF-_Y",
    "token_type": "bearer",
    "expires_in": 3600,
    "user": {
        "id": 1,
        "name": "Lura Waters DDS",
        "email": "consultant-1@iceberg-digital.co.uk",
        "email_verified_at": null,
        "created_at": "2021-12-11T20:13:38.000000Z",
        "updated_at": "2021-12-11T20:13:38.000000Z",
    }
}
```
#### `POST api/auth/logout`
Invalidates the current session

**Response**
```json
{
    "message": "User successfully signed out",
    "code": 200
}
```
#### `POST api/auth/refresh`
Generates new token with refreshed TTL

**Response**
```json
{
    "message": "Success",
    "code": 200,
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODg4OFwvYXBpXC9hdXRoXC9yZWZyZXNoIiwiaWF0IjoxNjM5MjU3NTMyLCJleHAiOjE2MzkyNjExMzMsIm5iZiI6MTYzOTI1NzUzMywianRpIjoiNzNHaUs1MThCd21sYURXdyIsInN1YiI6MSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.EzvUpNCl5uCXbf52aP0OG8UlKgCiaNZC0tp59QWXrB0",
    "token_type": "bearer",
    "expires_in": 3600,
    "user": {
        "id": 1,
        "name": "Lura Waters DDS",
        "email": "consultant-1@iceberg-digital.co.uk",
        "email_verified_at": null,
        "created_at": null,
        "updated_at": null
    }
}
```
#### `GET api/auth/user-profile`
Returns information of loggedin user

**Response**
```json
{
    "message": "Success",
    "code": 200,
    "data": {
        "id": 1,
        "name": "Lura Waters DDS",
        "email": "consultant-1@iceberg-digital.co.uk",
        "email_verified_at": null,
        "created_at": null,
        "updated_at": null
    }
}
```

### Appointment endpoints
#### `POST api/appointment/create`
**Parameters**

| Name             | Required   | Type       | Description                      |
| ---------------- | ---------- | ---------- | -------------------------------- |
| postcode         | `required` | `string`   | Valid UK postcode of destination |
| appointment_time | `required` | `datetime` | Time of appointment              |
| contact_email    | `required` | `string`   | Email of customer                |
| contact_name     | `required` | `string`   | Name of customer                 |
| contact_phone    | `required` | `numeric`  | Phone of customer                |

**Response**
```json
{
    "code": 201,
    "message": "Appointment has been successfully created.",
    "data": {
        "postcode": "CM20DL",
        "distance": 1341,
        "appointment_time": "2021-12-16 14:25",
        "estimated_departure_time": "2021-12-16T14:11:35.000000Z",
        "estimated_availability_time": "2021-12-16T15:38:25.000000Z",
        "contact_id": 2,
        "created_by": 1,
        "updated_at": "2021-12-11T20:00:41.000000Z",
        "created_at": "2021-12-11T20:00:41.000000Z",
        "id": 7
    }
}
```
#### `PATCH api/appointment/update/{appointmentId}`
**Parameters**
| Name             | Required   | Type       | Description                      |
| ---------------- | ---------- | ---------- | -------------------------------- |
| appointmentId    | `required` | `integer`  | ID of the appointment to update  |
| postcode         | `optional` | `string`   | Valid UK postcode of destination |
| appointment_time | `optional` | `datetime` | Time of appointment              |
| contact_email    | `optional` | `string`   | Email of customer                |
| contact_name     | `optional` | `string`   | Name of customer                 |
| contact_phone    | `optional` | `numeric`  | Phone of customer                |

**Response**
```json
{
    "code": 200,
    "message": "Appointment has been successfully updated.",
    "data": {
        "postcode": "CM20DL",
        "distance": 1341,
        "duration": 205,
        "appointment_time": "2021-12-13 05:30",
        "estimated_departure_time": "2021-12-13T05:16:35.000000Z",
        "estimated_availability_time": "2021-12-13T06:43:25.000000Z",
        "contact_id": 1
    }
}
```
#### `DELETE api/appointment/delete/{appointmentId}`
**Parameters**

| Name          | Required   | Type      | Description                     |
| ------------- | ---------- | --------- | ------------------------------- |
| appointmentId | `required` | `integer` | ID of the appointment to delete |

**Response**
```json
{
    "code": 200,
    "message": "Appointment has been successfully deleted."
}
```
#### `GET api/appointment/get-appointments`
**Parameters**

| Name       | Required   | Type       | Description |
| ---------- | ---------- | ---------- | ----------- |
| start_date | `optional` | `datetime` |             |
| end_date   | `optional` | `datetime` |             |

**Response**
```json
{
    "code": 200,
    "message": "Success",
    "data": [
        {
            "id": 2,
            "postcode": "CR03JF",
            "distance": 77548,
            "duration": 4316,
            "appointment_time": "2021-12-15 11:25:00",
            "estimated_departure_time": "2021-12-15 10:03:04",
            "estimated_availability_time": "2021-12-15 13:46:56",
            "contact_id": 2,
            "created_by": 1,
            "updated_by": null,
            "created_at": "2021-12-11T21:26:00.000000Z",
            "updated_at": "2021-12-11T21:26:00.000000Z"
        },
        {
            "id": 1,
            "postcode": "TN132HL",
            "distance": 65333,
            "duration": 2952,
            "appointment_time": "2021-12-14 14:25:00",
            "estimated_departure_time": "2021-12-14 13:25:48",
            "estimated_availability_time": "2021-12-14 16:24:12",
            "contact_id": 1,
            "created_by": 1,
            "updated_by": null,
            "created_at": "2021-12-11T21:25:28.000000Z",
            "updated_at": "2021-12-11T21:25:28.000000Z"
        }
    ]
}
```

## TODO
- [ ] Write unit tests 
- [ ] Enable email verification for registered users