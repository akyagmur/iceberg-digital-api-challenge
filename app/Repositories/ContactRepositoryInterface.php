<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

interface ContactRepositoryInterface
{
    public function firstOrCreate(array $attributes, array $values): Model;
}
