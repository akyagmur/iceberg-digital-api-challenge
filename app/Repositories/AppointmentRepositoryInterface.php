<?php

namespace App\Repositories;

interface AppointmentRepositoryInterface
{

    public function getUserAppointments(array $conditions = [], array $columns = ['*'], array $relations = [], array $appends = []);
}
