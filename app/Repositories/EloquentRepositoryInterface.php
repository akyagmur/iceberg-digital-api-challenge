<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface EloquentRepositoryInterface
 * @package App\Repositories
 */
interface EloquentRepositoryInterface
{
    /**
     * @param array $attributes
     * @return Model
     */
    public function create(array $attributes): Model;

    /**
     * @param int $modelId
     * @param array $columns
     * @param array $relations
     * @param array $appends
     * @return Model
     */
    public function findById(int $modelId, array $columns = ['*'], array $relations = [], array $appends = []): ?Model;

    /**
     * @param array $conditions
     * @param array $columns
     * @param array $relations
     * @param array $appends
     * @return Model
     */
    public function findOneBy(array $conditions = [], array $columns = ['*'], array $relations = [], array $appends = []): ?Model;

    /**
     * @param array $conditions
     * @param array $columns
     * @param array $relations
     * @return Collection
     */
    public function findBy(array $conditions = [], array $columns = ['*'], array $relations = []): ?Collection;


    /**
     * @param array $columns
     * @param array $relations
     * @return Collection
     */
    public function all(array $columns = ['*'], array $relations = []): Collection;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data): Model;

    /**
     * @param array $attributes
     * @param array $values
     */
    public function updateOrCreate(array $attributes, array $values = []): Model;
}
