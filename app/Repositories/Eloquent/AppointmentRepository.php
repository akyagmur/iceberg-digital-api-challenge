<?php

namespace App\Repositories\Eloquent;

use App\Models\Appointment;
use App\Repositories\AppointmentRepositoryInterface;

class AppointmentRepository extends BaseRepository implements AppointmentRepositoryInterface
{

    /**
     * AppointmentRepository constructor.
     *
     * @param Appointment $model
     */
    public function __construct(Appointment $model)
    {
        parent::__construct($model);
    }

    public function getUserAppointments(array $conditions = [], array $columns = ['*'], array $relations = [], array $appends = [])
    {
        $user = auth()->user();

        return $user->appointments()->where($conditions)->select($columns)->with($relations)->get();
    }
}
