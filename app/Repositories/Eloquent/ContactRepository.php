<?php

namespace App\Repositories\Eloquent;

use App\Models\Contact;
use App\Repositories\ContactRepositoryInterface;

class ContactRepository extends BaseRepository implements ContactRepositoryInterface
{

    /**
     * ContactRepository constructor.
     *
     * @param Contact $model
     */
    public function __construct(Contact $model)
    {
        parent::__construct($model);
    }

    public function firstOrCreate(array $attributes, array $values): Contact
    {
        return $this->model->firstOrCreate($attributes, $values);
    }
}
