<?php

namespace App\Rules;

use App\Helpers\Trip;
use App\Models\User;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;

class UserAvailable implements Rule, DataAwareRule
{
    /**
     * All of the data under validation.
     *
     * @var array
     */
    protected $data = [
        'postcode',
        'appointment_time'
    ];

    /**
     * @var User
     */
    private User $user;

    /**
     * @var integer
     */
    private $exceptAppointmentId;

    /**
     * Create a new rule instance.
     * @param User $user
     * @return void
     */
    public function __construct(int $exceptAppointmentId = null)
    {
        $this->user = auth()->user();
        $this->exceptAppointmentId = $exceptAppointmentId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $trip = new Trip('cm27pj', $this->data['postcode'], $this->data['appointment_time']);

        return $this->user->isAvailableBetween($trip->getEstimatedDepartureTime(), $trip->getEstimatedReturnTime(), $this->exceptAppointmentId);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You are not available for an appointment at given time.';
    }

    /**
     * Set the data under validation.
     *
     * @param  array  $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
