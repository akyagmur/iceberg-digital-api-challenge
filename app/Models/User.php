<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Http;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'created_by', 'id')->orderBy('appointment_time', 'desc');
    }

    /**
     * Checks if user is available between given dates
     * 
     * @param string $startDate
     * @param string $endDate
     * @param integer $exceptAppointmentId
     * @return bool
     *  */
    public function isAvailableBetween($startDate, $endDate, $exceptAppointmentId = null)
    {
        return $this
            ->appointments()
            ->where('id', '!=', $exceptAppointmentId)
            ->where(function ($query) use ($startDate, $endDate) {
                return $query->whereBetween('estimated_departure_time', [$startDate, $endDate])
                    ->orWhereBetween('estimated_availability_time', [$startDate, $endDate])
                    ->orWhereRaw('? BETWEEN estimated_departure_time and estimated_availability_time', [$startDate])
                    ->orWhereRaw('? BETWEEN estimated_departure_time and estimated_availability_time', [$endDate]);
            })
            ->exists()
            ? false
            : true;
    }
}
