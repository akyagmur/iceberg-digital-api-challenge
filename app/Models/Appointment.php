<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Appointment extends Model
{
    use HasFactory;

    protected $fillable = [
        'postcode',
        'distance',
        'duration',
        'appointment_time',
        'estimated_departure_time',
        'estimated_availability_time',
        'contact_id',
        'created_by',
        'updated_by',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = Auth::user() ? Auth::user()->id : 1;
        });

        static::updating(function ($model) {
            $model->updated_by = Auth::user() ? Auth::user()->id : 1;
        });
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
