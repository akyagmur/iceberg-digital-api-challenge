<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'created_by',
        'updated_by',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = Auth::user() ? Auth::user()->id : 1;
        });

        static::updating(function ($model) {
            $model->updated_by = Auth::user() ? Auth::user()->id : 1;
        });
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }
}
