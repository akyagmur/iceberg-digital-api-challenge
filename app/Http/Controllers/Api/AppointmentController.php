<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Trip;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CreateAppointmentRequest;
use App\Http\Requests\Api\UpdateAppointmentRequest;
use App\Repositories\Eloquent\AppointmentRepository;
use App\Repositories\Eloquent\ContactRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AppointmentController extends Controller
{
    private AppointmentRepository $appointmentRepository;
    private ContactRepository $contactRepository;

    public function __construct(AppointmentRepository $appointmentRepository, ContactRepository $contactRepository)
    {
        $this->appointmentRepository = $appointmentRepository;
        $this->contactRepository = $contactRepository;
    }

    public function create(CreateAppointmentRequest $request)
    {
        $appoitnmentTime = $request->appointment_time;

        $trip  = new Trip('cm27pj', $request->postcode, $appoitnmentTime);

        $contact = $this->contactRepository->firstOrCreate(
            [
                'email' => $request->contact_email
            ],
            [
                'name' => $request->contact_name,
                'phone' => $request->contact_phone,
            ]
        );

        $appointment = $this->appointmentRepository->create([
            'postcode' => preg_replace('/\s/', '', $request->postcode),
            'distance' => $trip->getDistance(),
            'duration' => $trip->getDuration(),
            'appointment_time' => $appoitnmentTime,
            'estimated_departure_time' => $trip->getEstimatedDepartureTime(),
            'estimated_availability_time' => $trip->getEstimatedReturnTime(),
            'contact_id' => $contact->id,
        ]);

        return response()->json([
            'code' => Response::HTTP_CREATED,
            'message' => 'Appointment has been successfully created.',
            'data' => $appointment
        ], Response::HTTP_CREATED);
    }

    public function update($appointmentId, UpdateAppointmentRequest $request)
    {
        $appointment = $this->appointmentRepository->findOneBy([
            ['id', $appointmentId],
            ['created_by', auth()->user()->id]
        ]);

        $data = [];

        if ($request->has('appointment_time') || $request->has('postcode')) {
            $appoitnmentTime = $request->appointment_time
                ? $request->appointment_time
                : $appointment->appointment_time;

            $postcode = $request->postcode
                ? preg_replace('/\s/', '', $request->postcode)
                : $appointment->postcode;

            $trip  = new Trip('cm27pj', $postcode, $appoitnmentTime);

            $data['postcode'] = $postcode;
            $data['distance'] = $trip->getDistance();
            $data['duration'] = $trip->getDuration();
            $data['appointment_time'] = $appoitnmentTime;
            $data['estimated_departure_time'] = $trip->getEstimatedDepartureTime();
            $data['estimated_availability_time'] = $trip->getEstimatedReturnTime();
        }

        if ($request->has('contact_email') || $request->has('contact_name') || $request->has('contact_phone')) {
            $contact = $this->contactRepository->updateOrCreate(
                [
                    'email' => $request->contact_email ?? $appointment->contact->email
                ],
                [
                    'name' => $request->contact_name ?? $appointment->contact->name,
                    'phone' => $request->contact_phone ?? $appointment->contact->phone,
                ]
            );

            $data['contact_id'] = $contact->id;
        }

        $this->appointmentRepository->update($appointment->id, $data);

        return response()->json([
            'code' => Response::HTTP_OK,
            'message' => 'Appointment has been successfully updated.',
            'data' => $data
        ], Response::HTTP_OK);
    }

    public function delete($appointmentId)
    {
        $appointment = $this->appointmentRepository->findOneBy([
            ['id', $appointmentId],
            ['created_by', auth()->user()->id]
        ]);

        $appointment->delete();


        return response()->json([
            'code' => Response::HTTP_OK,
            'message' => 'Appointment has been successfully deleted.'
        ], Response::HTTP_OK);
    }

    public function getAppointments(Request $request)
    {
        $request->validate([
            'start_date' => 'required_with:end_date|date',
            'end_date' => 'required_with:start_date|date|after_or_equal:start_date',
        ]);

        $conditions = [];

        if ($request->start_date && $request->end_date) {
            $conditions = [
                ['created_at', '>=', $request->start_date],
                ['created_at', '<=', $request->end_date],
            ];
        }

        return response()->json([
            'code' => Response::HTTP_OK,
            'message' => 'Success',
            'data' => $this->appointmentRepository->getUserAppointments($conditions)
        ], Response::HTTP_OK);
    }
}
