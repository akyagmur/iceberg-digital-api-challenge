<?php

namespace App\Http\Requests\Api;

use App\Rules\Postcode;
use App\Rules\UserAvailable;
use Illuminate\Foundation\Http\FormRequest;

class CreateAppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'postcode' => ['required', new Postcode],
            'appointment_time' => ['required', 'date_format:Y-m-d H:i', new UserAvailable],
            'contact_email' => 'required|email',
            'contact_name' => 'sometimes|string|min:3',
            'contact_phone' => 'sometimes|numeric'
        ];
    }
}
