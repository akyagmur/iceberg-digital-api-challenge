<?php

namespace App\Http\Requests\Api;

use App\Rules\Postcode;
use App\Rules\UserAvailable;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'postcode' => [new Postcode],
            'appointment_time' => ['date_format:Y-m-d H:i', new UserAvailable($this->appointmentId)],
            'contact_email' => 'email',
            'contact_name' => 'string|min:3',
            'contact_phone' => 'numeric'
        ];
    }
}
