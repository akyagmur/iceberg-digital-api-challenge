<?php

namespace App\Helpers;

use Carbon\CarbonImmutable;
use Exception;
use Illuminate\Support\Facades\Http;

/**
 * Helps to plan a trip between postcodes
 */
class Trip
{
    private $matrixDistanceData = [];
    private $destinationPostcode;
    private $originPostcode;
    private $arrivalTime;

    public function __construct($originPostcode, $destinationPostcode, $arrivalTime)
    {
        $this->setDestinationPostcode($destinationPostcode);
        $this->setOriginPostcode($originPostcode);
        $this->setArrivalTime($arrivalTime);

        $this->fetchDistanceMatrixData();
    }

    private function fetchDistanceMatrixData()
    {
        $data = Http::get(
            "https://maps.googleapis.com/maps/api/distancematrix/json",
            [
                'destinations' => $this->destinationPostcode,
                'origins' => $this->originPostcode,
                'key' => env('GOOGLE_MAPS_API_KEY')
            ]
        );

        if ($data["status"] !== "OK") {
            throw new Exception("No routes found!");
        }

        $this->matrixDistanceData = $data;
    }

    /** 
     * @return integer|null
     */
    public function getDistance()
    {
        return $this->matrixDistanceData["rows"][0]["elements"][0]["distance"]["value"] ?? null;
    }

    /** 
     * @return integer|null
     */
    public function getDuration()
    {
        return $this->matrixDistanceData["rows"][0]["elements"][0]["duration"]["value"] ?? null;
    }

    private function setDestinationPostcode($postcode)
    {
        $this->destinationPostcode = $postcode;
    }

    private function setOriginPostcode($postcode)
    {
        $this->originPostcode = $postcode;
    }

    private function setArrivalTime($arrivalTime)
    {
        $this->arrivalTime = CarbonImmutable::parse($arrivalTime);
    }

    /** 
     * @return CarbonImmutable
     */
    public function getEstimatedDepartureTime()
    {
        return $this->arrivalTime->subSeconds($this->getDuration())->subMinutes(10);
    }

    /** 
     * @return CarbonImmutable
     */
    public function getEstimatedReturnTime()
    {
        return $this->arrivalTime->addHour()->addSeconds($this->getDuration())->addMinutes(10);
    }
}
