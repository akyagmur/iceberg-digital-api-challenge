<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

/**
 * Helper class for postcode opeartions
 *  */
class Postcode
{
    const API_URL = 'https://api.postcodes.io';

    /**
     * Checks if postcode is valid
     * 
     * @param string $postcode
     * @return bool
     *  */
    public static function validate(string $postcode): bool
    {
        return Http::get(self::API_URL . "/postcodes/$postcode/validate")['result'];
    }

    /**
     * Returns a random postcode
     * 
     * @return string
     */
    public static function getRandom()
    {
        return Http::get(self::API_URL . "/random/postcodes")['result']['postcode'];
    }
}
