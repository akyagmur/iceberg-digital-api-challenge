<?php

use App\Http\Controllers\Api\AppointmentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->middleware(['api'])->group(function ($router) {
    Route::post('/login',       [AuthController::class, 'login'])->name('login');
    Route::post('/register',    [AuthController::class, 'register'])->name('register');
    Route::post('/logout',      [AuthController::class, 'logout'])->name('logout');
    Route::post('/refresh',     [AuthController::class, 'refresh'])->name('refresh');
    Route::get('/user-profile', [AuthController::class, 'userProfile'])->name('userProfile');
});

Route::prefix('appointment')->name('appointment.')->middleware(['refresh-jwt', 'api', 'auth'])->group(function ($router) {
    Route::post('/create',                      [AppointmentController::class, 'create'])->name('create');
    Route::patch('/update/{appointmentId}',     [AppointmentController::class, 'update'])->whereNumber('appointmentId')->name('update');
    Route::delete('/delete/{appointmentId}',    [AppointmentController::class, 'delete'])->whereNumber('appointmentId')->name('delete');
    Route::get('/get-appointments',             [AppointmentController::class, 'getAppointments'])->name('get_appointments');
});
